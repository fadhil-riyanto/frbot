#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

def parse_command(prefix, text):
    if text[:1] == prefix:
        rm_prefix = text[1:].split(' ')
        if len(rm_prefix) != 1:
            commands = rm_prefix[0]
            value = text[1:].split(' ', 1)[1]
            return {
                'value' : value,
                'command' : commands,
                'error' : None
            }
        else:
            commands = rm_prefix[0]
            # value = text[1:].split(' ', 1)[1]
            return {
                'value' : None,
                'command' : commands,
            'error' : None
            }

    else:
        return {
            'value' : None,
            'command' : None,
            'error' : "PREFIX_NDAK_VALID"
        }
