#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

import os

async def main(bot, message, input_telegram):
    if len(str(message)) > 4096:
        with open("temp/output.txt", "w+", encoding="utf8") as f:
            f.write(str(message))
        await message.reply_document("temp/output.txt", caption="Message Output")
        os.remove("temp/output.txt")
    else:
        await message.edit(f"```{str(message)}```")
