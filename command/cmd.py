#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

import sys,os
import env_sys
import platform

async def main(bot, message, input_telegram):

    cmds = ['.'.join(x.split('.')[:-1]) for x in os.listdir("command/") if os.path.isfile(os.path.join('command/', x))]
    
    reply = "Fadhil Userbot\n\n"
    reply += f"command tersedia : {cmdd}"
    
    await message.edit(reply)