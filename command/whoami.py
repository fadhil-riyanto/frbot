#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+


async def main(bot, message, input_telegram):
    raw_updatee = message.from_user
    if raw_updatee == None:
        reply = 'Maaf, atribute anda adalah tidak sebagai user biasa'
        await message.edit(reply)
    else:
        reply = '''👤 You
├ id: {id}
├ id grup: {chid}
├ kamu bot: {isbot}
├ nama pertama: {fname}
├ nama terakhir: {lname}
├ username: {uname}
├ DCID: {dc}
└ kode bahasa: {lang}'''.format(
    id = raw_updatee.id,
    chid=input_telegram['chat_id'],
    isbot=raw_updatee.is_bot,
    fname=raw_updatee.first_name,
    lname=raw_updatee.last_name,
    uname=raw_updatee.username,
    lang=raw_updatee.language_code,
    dc=raw_updatee.dc_id
)
        await message.edit(reply)
