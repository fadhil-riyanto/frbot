#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

import threading
import asyncio
import contextlib
import sys
from io import StringIO

class RunThread(threading.Thread):
    def __init__(self, func, args, kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
        super().__init__()

    def run(self):
        self.result = asyncio.run(self.func(*self.args, **self.kwargs))

def run_async(func, *args, **kwargs):
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        loop = None
    if loop and loop.is_running():
        thread = RunThread(func, args, kwargs)
        thread.start()
        thread.join()
        return thread.result
    else:
        return asyncio.run(func(*args, **kwargs))
@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old

async def main(bot, message, input_telegram):
    if input_telegram["text_crop"] == None:
        await message.edit(str("Maaf, silahkan masukkan kode"))
    else:
        # ini klo ntar crop nya ndak null

        with stdoutIO() as s:
            try:
                exec(input_telegram["text_crop"])
            except Exception as e:
                print("error alasan : " + str(e))
        reply = str(s.getvalue())
        await message.edit(reply)