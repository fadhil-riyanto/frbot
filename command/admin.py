#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

from pyrogram import errors

async def main(bot, message, input_telegram):

    try:
        if input_telegram['text_crop'] == None:
            idadminn = input_telegram['chat_id']
        else:
            idadminn = input_telegram['text_crop']
            
            
        # dapatkan admin
        tempdic = []
        tempdic_bot = []
        dapatkanadmin = await bot.get_chat_members(idadminn, filter="administrators")
        for ad in dapatkanadmin:
            if ad.status == 'administrator':
                if ad.user.is_bot == False:
                    ids = ad.user.id
                    fname = ad.user.first_name
                    lname = ad.user.last_name
                    if fname == None:
                        fname = ''
                    if lname == None:
                        lname = ''
                    nammee = fname + ' ' + lname
                    tempdic.append(f'''<a href="tg://user?id={ids}">{nammee}</a>''')
                elif ad.user.is_bot == True:
                    ids = ad.user.id
                    fname = ad.user.first_name
                    lname = ad.user.last_name
                    if fname == None:
                        fname = ''
                    if lname == None:
                        lname = ''
                    nammee = fname + ' ' + lname
                    #tempdic_bot.append(f'''<a href="tg://user?id={ids}">{nammee}</a>''')
                    tempdic_bot.append(f'''@{ad.user.username}''')
            elif ad.status == 'creator':
                ids = ad.user.id
                fname = ad.user.first_name
                lname = ad.user.last_name
                if fname == None:
                    fname = ''
                if lname == None:
                    lname = ''
                nammee = fname + ' ' + lname
                owerrrr = f'''<a href="tg://user?id={ids}">{nammee}</a>'''
                
            listadminjadi = []
            jumlahadministrator = len(tempdic)
            for addd in range(0, jumlahadministrator):
                if addd == jumlahadministrator - 1:
                    listadminjadi.append('└ ' + tempdic[addd])
                else:
                    listadminjadi.append('├ ' + tempdic[addd])
                    
            listadminbotjadi = []
            jumlahadministrator_bot = len(tempdic_bot)
            for addd in range(0, jumlahadministrator_bot):
                if addd == jumlahadministrator_bot - 1:
                    listadminbotjadi.append('└ ' + tempdic_bot[addd])
                else:
                    listadminbotjadi.append('├ ' + tempdic_bot[addd])
        adddd = '\r\n'.join(listadminjadi)
        bottt = '\r\n'.join(listadminbotjadi)
        
        # cek apakah adminnya cuma 1
        if adddd == None or adddd == '':
            if bottt == None or bottt == '':
                if owerrrr == None or owerrrr == '':
                    reply = 'tidak ada admin dsiini'
                else:
                    reply = 'Owner\r\n' + owerrrr
            else:
                reply = 'Owner\r\n' + owerrrr + '\r\n\r\nAdministrator bot\r\n' + bottt
        else:
            try:
                reply = 'Owner\r\n' + owerrrr + '\r\n\r\nAdministrator\r\n' + adddd + '\r\n\r\nAdministrator bot\r\n' + bottt
            except UnboundLocalError:
                reply = '\r\n\r\nAdministrator\r\n' + adddd + '\r\n\r\nAdministrator bot\r\n' + bottt
        await message.edit(reply)
        # with open('debugs', "w+", encoding="utf8") as fs:
        #     #dapatkanadmin = await bot.get_chat_members(idadminn, filter="administrators")
        #     fs.write(str(reply))
        
    except (errors.exceptions.bad_request_400.UsernameInvalid, errors.exceptions.bad_request_400.PeerIdInvalid):
        reply = "maaf, sepertinya username yang anda masukkan salah."
        await message.edit(reply)