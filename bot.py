#    +----------------------------------------------------------------------+
#    | Copyright (c) Fadhil riyanto                                         |
#    +----------------------------------------------------------------------+
#    | Anda diperbolehkan MERUBAH SELURUH KODE                              |
#    | tetapi dilarang merubah Copyright ini                                |
#    | ketika anda memublish hasil perubahan anda                           |
#    |                                                                      |
#    | Anda dilarang menjual SOURCE CODE ini untuk mendapatkan              |
#    | keuntungan pribadi.                                                  |
#    |                                                                      |
#    | software ini free, open source, semua akibat                         |
#    | penggunaan ditanggung pengguna. segala kerusakan                     |
#    | yang diakibatkan oleh software ini bukan salah pembuat               |
#    |                                                                      |
#    | Copyright masing masing pada pemilik library                         |
#    +----------------------------------------------------------------------+
#    | Authors: fadhil riyanto             <hi@fadhilriyanto.eu.org>        |
#    |                                    <fadhil.riyanto@gnuweeb.org>      |
#    +----------------------------------------------------------------------+

from pyrogram import Client, filters
import include.parsecommand as pc
import env_sys
import os
import importlib

app = Client("fadhil_riyanto")


@app.on_message(filters.me)
async def init(bot: Client, message):
	if message.text != None:
		parsec = pc.parse_command(env_sys.prefix, message.text)
		if parsec["error"] == "PREFIX_NDAK_VALID":
			preifxval = False
			commands = parsec["command"]
			value = parsec["value"]
		else:
			preifxval = True
			commands = parsec["command"]
			value = parsec["value"]

	input_telegram = {
		"valid_prefix":preifxval,
		"text_crop": value,
		"command": commands,
		"text_plain":message.text,
		"chat_id":message.chat.id
	}
	if input_telegram["valid_prefix"] == True:
		filelist = ['.'.join(x.split('.')[:-1]) for x in os.listdir("command/") if os.path.isfile(os.path.join('command/', x))]
		for filee in filelist:
			if filee.lower() == input_telegram["command"].lower():
				# await message.edit("command nemu pak")
				mymodule = importlib.import_module('command.' + filee.lower())
				await mymodule.main(bot, message, input_telegram)


	# await message.edit(str(filelist))



app.run()
